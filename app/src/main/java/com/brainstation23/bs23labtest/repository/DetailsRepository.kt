package com.brainstation23.bs23labtest.repository

import com.brainstation23.bs23labtest.network.domain.DetailsData
import com.brainstation23.bs23labtest.network.service.DetailsService
import com.brainstation23.bs23labtest.utils.DebugLogger
import javax.inject.Inject

class DetailsRepository @Inject constructor(
    val service: DetailsService,
    val logger: DebugLogger
) {
    suspend fun getDetails(name: String): DetailsData {
        return try {
            service.getUserDetails(name).detailsData
        } catch (e: Exception) {
            logger.e(throwable = e)
            DetailsData()
        }
    }
}