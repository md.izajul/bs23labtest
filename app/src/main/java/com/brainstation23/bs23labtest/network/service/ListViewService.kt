package com.brainstation23.bs23labtest.network.service

import com.brainstation23.bs23labtest.network.model.ModelListItem
import retrofit2.http.GET

interface ListViewService {
    @GET("/users")
    suspend fun getUserList(): List<ModelListItem>
}