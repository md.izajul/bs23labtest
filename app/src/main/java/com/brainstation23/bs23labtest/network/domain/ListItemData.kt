package com.brainstation23.bs23labtest.network.domain

data class ListItemData(
    val id: Int,
    val avatar: String,
    val username: String
)
