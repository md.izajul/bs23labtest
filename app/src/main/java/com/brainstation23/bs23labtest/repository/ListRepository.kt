package com.brainstation23.bs23labtest.repository

import com.brainstation23.bs23labtest.network.domain.ListItemData
import com.brainstation23.bs23labtest.network.model.ModelListItem
import com.brainstation23.bs23labtest.network.model.dataList
import com.brainstation23.bs23labtest.network.service.ListViewService
import com.brainstation23.bs23labtest.utils.DebugLogger
import javax.inject.Inject

class ListRepository @Inject constructor(
    val service: ListViewService,
    val logger: DebugLogger
) {

    suspend fun fetchUsers(): List<ListItemData> {
        try {
            return service.getUserList().dataList()
        } catch (e: Exception) {
            logger.e(throwable = e)
        }
        return ArrayList()
    }
}