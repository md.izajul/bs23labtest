package com.brainstation23.bs23labtest.network.service

import com.brainstation23.bs23labtest.network.model.ModelDetails
import retrofit2.http.GET
import retrofit2.http.Path

interface DetailsService {
    @GET("/users/{name}")
    suspend fun getUserDetails(@Path("name") name: String): ModelDetails
}