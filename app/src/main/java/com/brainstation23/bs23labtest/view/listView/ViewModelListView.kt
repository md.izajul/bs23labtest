package com.brainstation23.bs23labtest.view.listView

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brainstation23.bs23labtest.network.domain.ListItemData
import com.brainstation23.bs23labtest.network.model.ModelListItem
import com.brainstation23.bs23labtest.repository.ListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ViewModelListView @Inject constructor(
    val repository: ListRepository
) : ViewModel() {
    val dataList: MutableLiveData<List<ListItemData>> by lazy {
        viewModelScope.launch(Dispatchers.IO) {
            dataList.postValue(repository.fetchUsers())
        }
        MutableLiveData()
    }
}