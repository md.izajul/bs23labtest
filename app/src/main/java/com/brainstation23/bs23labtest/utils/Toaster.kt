package com.brainstation23.bs23labtest.utils

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Toaster @Inject constructor(private val appContext: Application) {

    fun show(message: String) = showToast(message)

    private fun showToast(message:String){
         Handler(Looper.getMainLooper()).post{
             Toast.makeText(appContext,message,Toast.LENGTH_SHORT).show()
         }
    }
}