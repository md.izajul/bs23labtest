package com.brainstation23.bs23labtest.view.listView

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.brainstation23.bs23labtest.R
import com.brainstation23.bs23labtest.databinding.FragmentListViewBinding
import com.brainstation23.bs23labtest.view.BaseFragment
import com.brainstation23.bs23labtest.view.detailsView.FragmentDetailsArgs
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FragmentListView : BaseFragment<FragmentListViewBinding>() {

    private val mViewModel: ViewModelListView by viewModels()

    @Inject
    lateinit var adapter: ListViewAdapter

    override fun setViewBind(inflater: LayoutInflater): FragmentListViewBinding =
        FragmentListViewBinding.inflate(layoutInflater)

    override fun initView() {

        binding.apply {
            viewModel = mViewModel
            lifecycleOwner = viewLifecycleOwner
            listRC.adapter = adapter
        }

    }

    override fun action() {

        mViewModel.dataList.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        adapter.clickListener.clickListener = {
            navigateTo(FragmentListViewDirections.actionListViewToDetailsView(it.username))
            /*findNavController().navigate(FragmentListViewDirections.actionListViewToDetailsView(it.username))*/
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.listRC.adapter = null
    }

}