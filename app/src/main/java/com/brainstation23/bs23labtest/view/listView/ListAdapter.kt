package com.brainstation23.bs23labtest.view.listView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.brainstation23.bs23labtest.databinding.ItemListBinding
import com.brainstation23.bs23labtest.network.domain.ListItemData
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class ListViewAdapter @Inject constructor(val clickListener: OnItemClickListener) :
    ListAdapter<ListItemData, ListViewAdapter.MyViewHolder>(ListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder =
        MyViewHolder.getHolder(parent)

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data, clickListener)
    }

    class MyViewHolder private constructor(private val binding: ItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ListItemData, clickListener: OnItemClickListener) {
            binding.data = item
            binding.executePendingBindings()
            binding.clickListener = clickListener
        }

        companion object {
            fun getHolder(parent: ViewGroup): MyViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemListBinding.inflate(layoutInflater, parent, false)
                return MyViewHolder(binding)
            }
        }
    }
}


class ListDiffCallback : DiffUtil.ItemCallback<ListItemData>() {

    override fun areItemsTheSame(oldItem: ListItemData, newItem: ListItemData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ListItemData, newItem: ListItemData): Boolean {
        return oldItem == newItem
    }
}

class OnItemClickListener @Inject constructor() {
    var clickListener: ((ListItemData) -> Unit)? = null

    fun onClick(data: ListItemData) {
        clickListener?.invoke(data)
    }
}