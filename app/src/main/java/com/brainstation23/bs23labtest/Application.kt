package com.brainstation23.bs23labtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Application : Application(){
    override fun onCreate() {
        if (BuildConfig.DEBUG) {
            // todo
        }
        super.onCreate()
    }
}