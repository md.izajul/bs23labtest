package com.brainstation23.bs23labtest.view.detailsView

import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.brainstation23.bs23labtest.databinding.FragmentDetailsBinding
import com.brainstation23.bs23labtest.view.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FragmentDetails : BaseFragment<FragmentDetailsBinding>() {

    private val mViewModel: ViewModelDetails by viewModels()
    private val args: FragmentDetailsArgs by navArgs()

    override fun forFirstTime() {
        super.forFirstTime()
        mViewModel.getDetails(args.userName)

    }
    override fun setViewBind(inflater: LayoutInflater): FragmentDetailsBinding =
        FragmentDetailsBinding.inflate(layoutInflater)

    override fun initView() {
        binding.viewModel = mViewModel
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun action() {

    }


}