package com.brainstation23.bs23labtest.di

import com.brainstation23.bs23labtest.BuildConfig
import com.brainstation23.bs23labtest.network.service.DetailsService
import com.brainstation23.bs23labtest.network.service.ListViewService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideHttpClient() = if (BuildConfig.DEBUG) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
            /// passing requirement heading parameters
            val request = chain.request().newBuilder().addHeader("User-Agent", BuildConfig.APPLICATION_ID).build()
            chain.proceed(request)
        }).build()
    } else {
        OkHttpClient.Builder().build()
    }

    @Singleton
    @Provides
    fun provideRetro(httpClient: OkHttpClient): Retrofit = Retrofit.Builder()
//        .addConverterFactory(MoshiConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.github.com/")
        .client(httpClient).build()

    @Provides
    @Singleton
    fun provideListService(retro: Retrofit): ListViewService =
        retro.create(ListViewService::class.java)

    @Provides
    @Singleton
    fun provideDetailsService(retro: Retrofit): DetailsService =
        retro.create(DetailsService::class.java)

}