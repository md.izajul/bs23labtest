package com.brainstation23.bs23labtest.view.detailsView

import androidx.databinding.ObservableParcelable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brainstation23.bs23labtest.network.domain.DetailsData
import com.brainstation23.bs23labtest.network.domain.ListItemData
import com.brainstation23.bs23labtest.repository.DetailsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ViewModelDetails @Inject constructor(
    val repository: DetailsRepository
) : ViewModel() {
    val details = ObservableParcelable(DetailsData())

    fun getDetails(user: String) = viewModelScope.launch(Dispatchers.IO) {
        details.set(repository.getDetails(user))
    }
}